﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using GZipTest.Dtos;

namespace GZipTest.FileProcessors
{
    public class Reader : IDisposable
    {
        private readonly FileStream _readStream;
        private readonly string _sourceFilePath;
        private readonly BlockingCollection<Block> _readQueue;
        
        private byte[] _readBuffer;

        public Reader(string sourceFilePath, BlockingCollection<Block> readQueue)
        {
            _readStream = File.Open(sourceFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            _sourceFilePath = sourceFilePath;
            _readQueue = readQueue;
        }

        public void StartReadThread()
        {
            var reader = new Thread(Read);
            reader.Start();
            Console.WriteLine($"Reader started. ThreadId: '{reader.ManagedThreadId}'.");
        }

        public void Read()
        {
            try
            {
                var fileLength = new FileInfo(_sourceFilePath).Length;
                bool shouldRead = true;
                int blockId = 0;

                _readBuffer = new byte[Constants.BufferSize];

                while (shouldRead)
                {
                    bool isStartOfChunk = true;
                    long nextChunkStreamPosition = 0;
                    var readableDataSize = 0;
                    var headerBytesIndex = 0;

                    for (int i = 0; i < _readBuffer.Length; i++)
                    {
                        var b = _readStream.ReadByte();
                        if (b == Constants.GzipHeaderBytes[headerBytesIndex])
                        {
                            headerBytesIndex++;
                        }
                        else
                        {
                            headerBytesIndex = 0;
                        }

                        _readBuffer[i] = (byte) b;

                        if (headerBytesIndex == Constants.GzipHeaderBytes.Length - 1)
                        {
                            if (isStartOfChunk)
                            {
                                headerBytesIndex = 0;
                                isStartOfChunk = false;
                                continue;
                            }
                            readableDataSize = i - 9;
                            nextChunkStreamPosition = _readStream.Position - 9;

                            break;
                        }
                        if (_readStream.Position == fileLength)
                        {
                            shouldRead = false;
                            readableDataSize = i;
                            break;
                        }
                    }

                    var dataToDecompress = new byte[readableDataSize];
                    Array.Copy(_readBuffer, 0, dataToDecompress, 0, readableDataSize);

                    _readQueue.Add(new Block(blockId, dataToDecompress));

                    _readStream.Position = nextChunkStreamPosition;
                    blockId++;
                }
                _readQueue.CompleteAdding();
            }
            catch (Exception exception)
            {
                Environment.ExitCode = -1;
                Console.WriteLine(exception);
                _readStream?.Dispose();
            }
        }

        public void Dispose()
        {
            _readStream.Dispose();
        }
    }
}
