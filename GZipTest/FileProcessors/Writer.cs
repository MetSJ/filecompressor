﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GZipTest.Dtos;

namespace GZipTest.FileProcessors
{
    public class Writer : IDisposable
    {
        private readonly FileStream _writeFileStream;
        private readonly BlockingCollection<Block> _writeQueue;
        private readonly ManualResetEvent _onFinish;
        private readonly CancellationToken _cancellationToken;
        private readonly int _tryPeekTimeoutMs = 5000;

        private readonly Dictionary<long, byte[]> _cache = new Dictionary<long, byte[]>();
        private int _awaitedBlockId = 0;

        public Writer(string targetFilePath, BlockingCollection<Block> writeQueue, ManualResetEvent onFinish, CancellationToken cancellationToken)
        {
            _writeFileStream = File.Open(targetFilePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
            _writeQueue = writeQueue;
            _onFinish = onFinish;
            _cancellationToken = cancellationToken;
        }

        public void StartListenerThread()
        {
            var listener = new Thread(Write);
            listener.Start();
            Console.WriteLine($"Writer started. ThreadId: '{listener.ManagedThreadId}'.");
        }

        public void Write()
        {
            try
            {
                while (!_writeQueue.IsCompleted)
                {
                    Block writeItem;
                    if (_writeQueue.TryTake(out writeItem, _tryPeekTimeoutMs, _cancellationToken))
                    {
                        if (writeItem.BlockId == _awaitedBlockId)
                        {
                            PushToFileStream(writeItem.Bytes);

                            _awaitedBlockId++;
                            WriteCachedItemsOrdered();
                        }
                        else
                        {
                            _cache.Add(writeItem.BlockId, writeItem.Bytes);
                        }
                    }
                }

                _onFinish.Set();
            }
            catch (OperationCanceledException)
            {
                Environment.ExitCode = -1;
                Console.WriteLine("Writing thread cancelled. Closing writeStream.");
            }
            catch (Exception exception)
            {
                Environment.ExitCode = -1;
                Console.WriteLine(exception);
            }
            finally
            {
                _writeFileStream?.Dispose();
            }
        }

        private void WriteCachedItemsOrdered()
        {
            bool searchInCache = true;
            while (searchInCache)
            {
                byte[] cachedItem;
                if (_cache.TryGetValue(_awaitedBlockId, out cachedItem))
                {
                    _cache.Remove(_awaitedBlockId);
                    PushToFileStream(cachedItem);
                    
                    _awaitedBlockId++;
                }
                else
                {
                    searchInCache = false;
                }
            }
        }

        private void PushToFileStream(byte[] bytes)
        {
            _writeFileStream.Write(bytes, 0, bytes.Length);
        }

        public void Dispose()
        {
            _writeFileStream?.Dispose();
            _writeQueue.Dispose();
        }
    }
}
