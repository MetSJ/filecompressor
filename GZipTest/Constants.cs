﻿using System;

namespace GZipTest
{
    public static class Constants
    {
        public const int BufferSize = 1048576;

        public static readonly byte[] GzipHeaderBytes = { 0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00 };

        public static readonly int NumberOfCores = Environment.ProcessorCount;
    }
}
