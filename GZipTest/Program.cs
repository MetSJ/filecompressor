﻿using System;
using System.Diagnostics;
using System.IO;
using GZipTest.Compression;
using GZipTest.Decompression;

namespace GZipTest
{
    public class Program
    {
        private const string CmdUsageMsg =
            "Use: compress [original file name] [archive file name]\n" +
            "or: decompress [archive file name] [decompressing file name]";

        static int Main(string[] args)
        {
            Environment.ExitCode = 0;
            var watch = Stopwatch.StartNew();
            watch.Start();

            try
            {
                bool isCompressAction;
                string sourceFilePath, targetFilePath;
                GetRunParams(args, out isCompressAction, out sourceFilePath, out targetFilePath);

                if (File.Exists(targetFilePath))
                {
                    Console.WriteLine($"TargetFile exists. Deleting file before processing.");
                    File.Delete(targetFilePath);
                }

                if (isCompressAction)
                {
                    using (var compressionManager = new CompressionManager(sourceFilePath, targetFilePath))
                    {
                        compressionManager.Compress();
                    }
                }
                else
                {
                    using (var decompressor = new DecompressionManager(sourceFilePath, targetFilePath))
                    {
                        decompressor.Decompress();
                    }
                }

                Console.WriteLine($"Operation finished in: '{watch.Elapsed}'.");

                return Environment.ExitCode;
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error was raised with message: '{exception.Message}'.");
                return -1;
            }
        }

        private static void GetRunParams(string[] args, out bool isCompressAction, out string sourceFilePath, out string targetFilePath)
        {
            if (args.Length != 3)
            {
                Environment.ExitCode = -1;
                throw new ArgumentException("Not all arguments set.\n" + CmdUsageMsg);
            }

            isCompressAction = false;
            switch (args[0].ToLower())
            {
                case "decompress":
                    break;
                case "compress":
                    isCompressAction = true;
                    break;
                default:
                    Environment.ExitCode = -1;
                    throw new ArgumentException("Cannot define action.\n" + CmdUsageMsg);
            }

            sourceFilePath = args[1];
            targetFilePath = args[2];
            if (!File.Exists(sourceFilePath))
            {
                Environment.ExitCode = -1;
                throw new ArgumentException(
                    $"File specified as source does not exist, please check file path.Requested file: '{args[0]}'.");
            }
        }
    }
}
