﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Threading;
using GZipTest.Dtos;

namespace GZipTest.Decompression
{
    public class Decompressor
    {
        private readonly BlockingCollection<Block> _itemsToProcessQueue;
        private readonly BlockingCollection<Block> _processedItemsQueue;

        public Decompressor(
            BlockingCollection<Block> itemsToProcessQueue,
            BlockingCollection<Block> processedItemsQueue
            )
        {
            _itemsToProcessQueue = itemsToProcessQueue;
            _processedItemsQueue = processedItemsQueue;
        }

        public ManualResetEvent[] CreateDecompressionThreads(int requestedThreadsCount)
        {
            var onFinishEvents = new ManualResetEvent[requestedThreadsCount];

            for (int i = 0; i < requestedThreadsCount; i++)
            {
                var onFinish = new ManualResetEvent(false);
                onFinishEvents[i] = onFinish;
                var decompressor = new Thread(() => Decompress(onFinish));
                decompressor.Start();
            }
            return onFinishEvents;
        }

        private void Decompress(ManualResetEvent onFinish)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine($"Decompression started. ThreadId: '{threadId}'.");

            try
            {
                var decompressedBytesBuffer = new byte[Constants.BufferSize];

                while (!_itemsToProcessQueue.IsCompleted)
                {
                    Block itemToProcess;
                    if(!_itemsToProcessQueue.TryTake(out itemToProcess))
                    {
                        continue;
                    }
                    
                    var decompressedChunk = Decompress(itemToProcess.Bytes, decompressedBytesBuffer);

                    _processedItemsQueue.Add(new Block(itemToProcess.BlockId, decompressedChunk));
                }

                Console.WriteLine($"Decompression finished. ThreadId: '{threadId}'.");
                onFinish.Set();
            }
            catch (Exception exception)
            {
                Environment.ExitCode = -1;
                Console.WriteLine(exception);
                onFinish.Set();
            }
        }

        private static byte[] Decompress(byte[] compressedBytes, byte[] decompressedBytesBuffer)
        {
            var inputStream = new MemoryStream(compressedBytes.Length);
            inputStream.Write(compressedBytes, 0, compressedBytes.Length);
            inputStream.Position = 0;

            var decompressionStream = new GZipStream(inputStream, CompressionMode.Decompress, false);
            var decompressedBytesCount = decompressionStream.Read(decompressedBytesBuffer, 0, Constants.BufferSize);
            
            decompressionStream.Dispose();

            var decompressedData = new byte[decompressedBytesCount];
            Array.Copy(decompressedBytesBuffer, 0, decompressedData, 0, decompressedBytesCount);
            
            return decompressedData;
        }
    }
}
