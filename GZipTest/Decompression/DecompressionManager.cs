﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using GZipTest.Dtos;
using GZipTest.FileProcessors;

namespace GZipTest.Decompression
{
    public class DecompressionManager : IDisposable
    {
        private readonly string _sourceFilePath;
        private readonly string _targetFilePath;
        private readonly CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();

        public DecompressionManager(string sourceFilePath, string targetFilePath)
        {
            _sourceFilePath = sourceFilePath;
            _targetFilePath = targetFilePath;
        }

        public void Decompress()
        {
            var readerQueue = new BlockingCollection<Block>();
            var writerQueue = new BlockingCollection<Block>();
            var onWriteFinished = new ManualResetEvent(false);

            var cancellationToken = _cancelTokenSource.Token;

            using (var writer = new Writer(_targetFilePath, writerQueue, onWriteFinished, cancellationToken))
            {
                try
                {
                    writer.StartListenerThread();

                    var reader = new Reader(_sourceFilePath, readerQueue);
                    reader.StartReadThread();

                    var decompressor = new Decompressor(readerQueue, writerQueue);
                    var onAllFinished = decompressor.CreateDecompressionThreads(Constants.NumberOfCores);

                    foreach (var onFinished in onAllFinished)
                    {
                        onFinished.WaitOne();
                    }
                    writerQueue.CompleteAdding();

                    onWriteFinished.WaitOne();

                    reader.Dispose();
                }
                catch (Exception)
                {
                    _cancelTokenSource?.Cancel();
                    throw;
                }
            }
        }

        public void Dispose()
        {
            _cancelTokenSource?.Dispose();
        }
    }
}
