namespace GZipTest.Dtos
{
    public class Block
    {
        public Block(long id, byte[] bytes)
        {
            BlockId = id;
            Bytes = bytes;
        }

        public long BlockId;
        public byte[] Bytes;
    }
}