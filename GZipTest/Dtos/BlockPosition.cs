namespace GZipTest.Dtos
{
    public class BlockPosition
    {
        public long BlockId;
        public long ReadFrom;
        public long ReadTo;
    }
}