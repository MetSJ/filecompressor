﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Threading;
using GZipTest.Dtos;

namespace GZipTest.Compression
{
    public class Compressor
    {
        private readonly string _sourceFilePath;
        private readonly ConcurrentQueue<BlockPosition> _contextQueue;
        private readonly BlockingCollection<Block> _writeQueue;

        public Compressor(
            string sourceFilePath,
            ConcurrentQueue<BlockPosition> contextQueue,
            BlockingCollection<Block> writeQueue
            )
        {
            _sourceFilePath = sourceFilePath;
            _contextQueue = contextQueue;
            _writeQueue = writeQueue;
        }

        public ManualResetEvent[] CreateCompressionThreads(int requestedThreadsCount)
        {
            var onFinishEvents = new ManualResetEvent[requestedThreadsCount];

            for (int i = 0; i < requestedThreadsCount; i++)
            {
                var onFinish = new ManualResetEvent(false);
                onFinishEvents[i] = onFinish;
                var thread = new Thread(() => Compress(onFinish));
                thread.Start();
            }
            return onFinishEvents;
        }

        public void Compress(ManualResetEvent onFinished)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine($"Compressor started. ThreadId: '{threadId}'.");

            FileStream readStream = null;
            MemoryStream memoryStream = null;
            try
            {
                readStream = File.Open(_sourceFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                memoryStream = new MemoryStream(Constants.BufferSize);
                byte[] readStreamBuffer = new byte[Constants.BufferSize];

                BlockPosition context;
                while (_contextQueue.TryDequeue(out context))
                {
                    readStream.Seek(context.ReadFrom, SeekOrigin.Begin);
                    
                    long bytesToRead = context.ReadTo - context.ReadFrom;

                    readStream.Read(readStreamBuffer, 0, (int)bytesToRead);
                    byte[] compressedBytes = Compress(readStreamBuffer, (int)bytesToRead, memoryStream);

                    _writeQueue.Add(new Block(context.BlockId, compressedBytes));
                }

                Console.WriteLine($"Compression finished. ThreadId: '{threadId}'.");
                onFinished.Set();
            }
            catch (Exception exception)
            {
                Environment.ExitCode = -1;
                Console.WriteLine(exception);
                readStream?.Dispose();
                memoryStream?.Dispose();
                onFinished.Set();
            }
        }

        private static byte[] Compress(byte[] buffer, int bytesToRead, MemoryStream memoryStream)
        {
            memoryStream.Position = 0;
            var compressionStream = new GZipStream(memoryStream, CompressionMode.Compress, true);
            
            compressionStream.Write(buffer, 0, bytesToRead);

            compressionStream.Dispose();

            var compressedBytesCount = (int)memoryStream.Position;

            var compressedBytes = new byte[compressedBytesCount];
            memoryStream.Position = 0;
            memoryStream.Read(compressedBytes, 0, compressedBytesCount);

            return compressedBytes;
        }
    }
}
