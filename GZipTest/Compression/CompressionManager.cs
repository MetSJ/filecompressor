﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GZipTest.Dtos;
using GZipTest.FileProcessors;

namespace GZipTest.Compression
{
    public class CompressionManager : IDisposable
    {
        private readonly string _sourceFilePath;
        private readonly string _targetFilePath;
        private readonly ConcurrentQueue<BlockPosition> _contextsQueue = new ConcurrentQueue<BlockPosition>();
        private readonly CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();

        public CompressionManager(string sourceFilePath, string targetFilePath)
        {
            _sourceFilePath = sourceFilePath;
            _targetFilePath = targetFilePath;
        }

        public void Compress()
        {
            var writeQueue = new BlockingCollection<Block>();
            var onWriteFinished = new ManualResetEvent(false);

            var cancellationToken = _cancelTokenSource.Token;

            using (var writer = new Writer(_targetFilePath, writeQueue, onWriteFinished, cancellationToken))
            {
                try
                {
                    var cors = Constants.NumberOfCores;
                    Console.WriteLine($"Compressing file: '{_sourceFilePath}', threads count: '{cors}'");
                    
                    var fileLength = new FileInfo(_sourceFilePath).Length;

                    var chunks = CalculateChunksRegardingBufferSize(cors, fileLength);

                    for (int i = 0; i < chunks.Count; i++)
                    {
                        _contextsQueue.Enqueue(chunks[i]);
                    }

                    writer.StartListenerThread();

                    var compressor = new Compressor(_sourceFilePath, _contextsQueue, writeQueue);
                    
                    var onFinishEvents = compressor.CreateCompressionThreads(cors);

                    foreach (var finishEvent in onFinishEvents)
                    {
                        finishEvent.WaitOne();
                    }

                    writeQueue.CompleteAdding();
                    onWriteFinished.WaitOne();
                }
                catch (Exception)
                {
                    _cancelTokenSource?.Cancel();
                    throw;
                }
            }
        }

        public Dictionary<long, BlockPosition> CalculateChunksRegardingBufferSize(int threadsCount, long inputFileLength)
        {
            var result = new Dictionary<long, BlockPosition>();

            long lowerByteIndex = 0;
            long shift = Constants.BufferSize;
            long upperByteIndex = shift;
            for (int i = 0; lowerByteIndex < inputFileLength; i++)
            {
                if (upperByteIndex > inputFileLength)
                {
                    upperByteIndex = inputFileLength;
                }

                result.Add(i, new BlockPosition
                {
                    BlockId = i,
                    ReadFrom = lowerByteIndex,
                    ReadTo = upperByteIndex
                });
                lowerByteIndex += shift;
                upperByteIndex = lowerByteIndex + shift;
            }
            
            return result;
        }

        public void Dispose()
        {
            _cancelTokenSource?.Dispose();
        }
    }
}
